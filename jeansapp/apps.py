from django.apps import AppConfig


class JeansappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'jeansapp'
