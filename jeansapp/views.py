from django.shortcuts import render
from .models import Product,Logo
# Create your views here.


def index(request):
    product = Product.objects.all()
    logo = Logo.objects.all()
    return render(request,'index.html',{'product':product,'logo':logo})


def product_detail(request,slug):
    products =Product.objects.get(slug__iexact=slug)
    return render(request,'product_detail.html',{'products':products})




def index_ru(request):
    product = Product.objects.all()
    logo = Logo.objects.all()
    return render(request,'indexru.html',{'product':product,'logo':logo})

def index_uz(request):
    product = Product.objects.all()
    logo = Logo.objects.all()
    return render(request,'indexuz.html',{'product':product,'logo':logo})