from django.db import models
from django.shortcuts import reverse

# Create your models here.




class Logo(models.Model):
    image = models.ImageField(null=True,blank=True)






class Product(models.Model):
    name = models.CharField(max_length=50,)
    slug = models.SlugField(unique=True,null=True)
    description_en = models.TextField(null=True,blank=True)
   
    image = models.ImageField(null=True,blank=True)


    def __str__(self):
        return self.name


    def get_absolute_url(self):
        return reverse('product_detail_url' , kwargs={'slug':self.slug})