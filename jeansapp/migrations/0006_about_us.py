# Generated by Django 5.0 on 2024-02-27 05:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('jeansapp', '0005_logo'),
    ]

    operations = [
        migrations.CreateModel(
            name='About_us',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('text', models.TextField()),
            ],
        ),
    ]
