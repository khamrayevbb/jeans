from django.urls import path
from . import views

urlpatterns = [
    path('',views.index, name='index'),
    path('product/<slug:slug>/', views.product_detail, name="product_detail_url"),
    path('ru/',views.index_ru, name='indexru'),
    path('uz/',views.index_uz, name='indexuz')

]